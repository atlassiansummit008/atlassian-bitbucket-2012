# German translation of Python Sudoku
# Copyright (C) 2005
# This file is distributed under the same license as the Python Sudoku package.
# Mario Blättermann <mbl103@arcor.de>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: Python Sudoku 0.12\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-09-18 23:30+0200\n"
"PO-Revision-Date: 2006-09-17 11:37+0100\n"
"Last-Translator: Mario Blättermann <mbl103@arcor.de>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /usr/lib/python2.4/optparse.py:332
#, python-format
msgid "usage: %s\n"
msgstr "Anwendung: %s\n"

#: /usr/lib/python2.4/optparse.py:351
msgid "Usage"
msgstr "Anwendung"

#: /usr/lib/python2.4/optparse.py:357
msgid "integer"
msgstr "integer"

#: /usr/lib/python2.4/optparse.py:358
msgid "long integer"
msgstr "long integer"

#: /usr/lib/python2.4/optparse.py:359
msgid "floating-point"
msgstr "Fliesskommazahl"

#: /usr/lib/python2.4/optparse.py:360
msgid "complex"
msgstr "complex"

#: /usr/lib/python2.4/optparse.py:368
#, python-format
msgid "option %s: invalid %s value: %r"
msgstr "Option %s: Ungültiger Wert: %r"

#: /usr/lib/python2.4/optparse.py:376
#, python-format
msgid "option %s: invalid choice: %r (choose from %s)"
msgstr "Option %s: Ungültige Auswahl: %r (Wähle aus %s)"

#: /usr/lib/python2.4/optparse.py:1141
msgid "show this help message and exit"
msgstr "Hilfe anzeigen und beenden"

#: /usr/lib/python2.4/optparse.py:1146
msgid "show program's version number and exit"
msgstr "Versionsnummer anzeigen und beenden"

#: /usr/lib/python2.4/optparse.py:1169
msgid "%prog [options]"
msgstr "%prog [Optionen]"

#: /usr/lib/python2.4/optparse.py:1379 /usr/lib/python2.4/optparse.py:1418
#, python-format
msgid "%s option requires an argument"
msgstr "%s Option braucht ein Argument "

#: /usr/lib/python2.4/optparse.py:1381 /usr/lib/python2.4/optparse.py:1420
#, python-format
msgid "%s option requires %d arguments"
msgstr "%s Option braucht %d Argumente"

#: /usr/lib/python2.4/optparse.py:1390
#, python-format
msgid "%s option does not take a value"
msgstr "%s Option enthält keinen Wert"

#: /usr/lib/python2.4/optparse.py:1407 /usr/lib/python2.4/optparse.py:1561
#, python-format
msgid "no such option: %s"
msgstr "Ungültige Option: %s"

#: /usr/lib/python2.4/optparse.py:1507
msgid "options"
msgstr "Optionen"

#: /usr/lib/python2.4/optparse.py:1565
#, python-format
msgid "ambiguous option: %s (%s?)"
msgstr "Zweideutige Option: %s (%s?)"

#: pysdk-gui.py:83
msgid ""
"\n"
"  %prog [GUI Options] [Print Options] [PDF Options] [Image Options] [INPUT.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file"
msgstr ""

#: pysdk-gui.py:92
msgid "show the modules not found"
msgstr "Nicht gefundene Module anzeigen"

#: pysdk-gui.py:96 pysdk-image.py:92 pysdk-pdf.py:98 pysdk.py:100
msgid "information about what is sudoku"
msgstr "Informationen über Sudoku"

#: pysdk-gui.py:99 pysdk.py:103
msgid "Creation Options"
msgstr ""

#: pysdk-gui.py:103 pysdk.py:107
#, python-format
msgid "set the difficulty of the sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" is the default)"
msgstr "setzt den Schwierigkeitsgrad (\"hard\", \"normal\", \"easy\") (\"%s\" ist Standard)"

#: pysdk-gui.py:107 pysdk.py:114
#, python-format
msgid "set the handicap of the sudoku (0 = insane, 1 = insane + 1 extra number, etc) (%d is the default)"
msgstr "Setzt das Handicap des Spiels (0 = Maximum, 1 = eine Stufe tiefer usw. ) (%d ist Standard)"

#: pysdk-gui.py:111 pysdk.py:118
#, python-format
msgid "set the region width. The board will be HxW grid of WxH grids (%d is the default)"
msgstr "Breite des Spielfelds, das Feld besteht aus HxW Innenquadraten zu je HxW Zellen (%d ist Standard)"

#: pysdk-gui.py:115 pysdk.py:122
#, python-format
msgid "set the region height. The board will be HxW grid of WxH grids (%d is the default)"
msgstr "Höhe des Spielfelds, das Feld besteht aus HxW Innenquadraten zu je HxW Zellen (%d ist Standard)"

#: pysdk-gui.py:119 pysdk-pdf.py:164
msgid "Print Options"
msgstr "Druckoptionen"

#: pysdk-gui.py:121 pysdk-pdf.py:166
msgid "set the command to print (not value set)"
msgstr "Druckbefehl (kein Standardbefehl gesetzt)"

#: pysdk-gui.py:122 pysdk-pdf.py:167
#, python-format
msgid "set the command to print (\"%s\" is the default)"
msgstr "Druckbefehl (\"%s\" ist Standard)"

#: pysdk-gui.py:130
msgid "Print and PDF Options"
msgstr "ruck- und PDF-Optionen"

#: pysdk-gui.py:134 pysdk-pdf.py:105
#, python-format
msgid "set the page size (\"A4\", \"LEGAL\", \"LETTER\", etc) (\"%s\" is the default)"
msgstr "Papierformat festlegen (\"A4\", \"LEGAL\", \"LETTER\", usw.)(\"%s\" ist Standard)"

#: pysdk-gui.py:138 pysdk-pdf.py:109
msgid "don't draw the title text"
msgstr "Keine Titelzeile drucken"

#: pysdk-gui.py:142 pysdk-pdf.py:113
#, python-format
msgid "set the title font (\"%s\" is the default)"
msgstr "Schriftart der Titelzeile (\"%s\" ist Standard)"

#: pysdk-gui.py:146 pysdk-pdf.py:117
#, python-format
msgid "set the title colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "Schriftfarbe der Titelzeile (\"black\", \"blue\", etc) (\"%s\" ist Standard)"

#: pysdk-gui.py:150 pysdk-pdf.py:121
#, python-format
msgid "set the title font size (%d is the default)"
msgstr "Schriftgrösse der Titelzeile (%d ist Standard)"

#: pysdk-gui.py:154 pysdk-pdf.py:125
msgid "don't draw the filename"
msgstr "Dateiname nicht anzeigen"

#: pysdk-gui.py:158 pysdk-pdf.py:129
#, python-format
msgid "set the filename font (\"%s\" is the default)"
msgstr "Schriftart des Dateinamens (\"%s\" ist Standard)"

#: pysdk-gui.py:162 pysdk-pdf.py:133
#, python-format
msgid "set the filename colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "Farbe des Dateinamens (\"black\", \"blue\", usw. ) (\"%s\" ist Standard)"

#: pysdk-gui.py:166 pysdk-pdf.py:137
#, python-format
msgid "set the filename size (%d is the default)"
msgstr "Schriftgrösse des Dateinamens (%d ist Standard)"

#: pysdk-gui.py:169 pysdk-pdf.py:140
msgid "show 4 sudokus instead of 1"
msgstr "Statt einem vier Sudokus zeigen"

#: pysdk-gui.py:173 pysdk-pdf.py:144
msgid "show valids fonts"
msgstr "Verfügbare Schriften anzeigen"

#: pysdk-gui.py:177 pysdk-image.py:95
msgid "Image Options"
msgstr "Bildoptionen"

#: pysdk-gui.py:181 pysdk-image.py:99
#, python-format
msgid "set the image format (\"png\", \"jpeg\", etc) (\"%s\" is the default)"
msgstr "Bildformat \"png\", \"jpeg\", usw. ) (\"%s\" ist Standard)"

#: pysdk-gui.py:185 pysdk-image.py:103
#, python-format
msgid "set the image width in pixels (%d is the default)"
msgstr "Bildbreite in Pixeln angeben (%d ist Standard)"

#: pysdk-gui.py:189 pysdk-image.py:107
#, python-format
msgid "set the image height in pixels (%d is the default)"
msgstr "Bildhöhe in Pixeln angeben (%d ist Standard)"

#: pysdk-gui.py:193 pysdk-image.py:111
msgid "create a transparent image without background (if the format doesn't support transparency a black background is created)"
msgstr "Bild mit transparentem Hintergrund erzeugen. (Falls vom Dateiformatnicht unterstützt, wird ein schwarzer Hintergrund erzeugt.)"

#: pysdk-gui.py:197 pysdk-image.py:115
#, python-format
msgid "set the image background (\"white\", \"blue\", etc) (\"%s\" is the default)"
msgstr "Hintergrundfarbe (\"white\", \"blue\", usw. ) (\"%s\" ist Standard)"

#: pysdk-gui.py:202
msgid "Print, PDF and Image Options"
msgstr "Optionen für Druck, PDF und Bild"

#: pysdk-gui.py:207
#, python-format
msgid "set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "Linienfarbe (\"black\", \"blue\", etc) (\"%s\" ist Standard für Bild und \"%s\" für PDF/Drucken)"

#: pysdk-gui.py:211
#, python-format
msgid "set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "Schriftart der Zahlen (absoluter Pfad oder relativ zum Skript) (\"%s\" ist der Standard für Bilder und  \"%s\" für PDF/Drucken)"

#: pysdk-gui.py:216
#, python-format
msgid "set the font size for the numbers (%d is the default for image and %d for PDF/Print)"
msgstr "Schriftgrösse der Zahlen (\"%s\" ist der Standard für Bilder und  \"%s\" für PDF/Drucken)"

#: pysdk-gui.py:221
#, python-format
msgid "set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "Ziffernfarbe (\"black\", \"blue\", etc) (\"%s\" ist der Standard für Bilder und \"%s\" für PDF/Drucken)"

#: pysdk-gui.py:224 pysdk-image.py:135 pysdk-pdf.py:174 pysdk.py:125
msgid "Visualization Options"
msgstr "Visualisierungsoptionen"

#: pysdk-gui.py:226 pysdk-image.py:137 pysdk-pdf.py:176 pysdk.py:127
msgid "show letters instead of numbers > 9 (default)"
msgstr "Buchstaben anstelle von Ziffern > 9 (Standard)"

#: pysdk-gui.py:227 pysdk-image.py:138 pysdk-pdf.py:177 pysdk.py:128
msgid "show only numbers"
msgstr "Nur Ziffern zeigen"

#: pysdk-gui.py:229 pysdk-image.py:140 pysdk-pdf.py:179 pysdk.py:130
msgid "show letters instead of numbers > 9"
msgstr "Buchstaben anstelle von Ziffern > 9 (Standard)"

#: pysdk-gui.py:230 pysdk-image.py:141 pysdk-pdf.py:180 pysdk.py:131
msgid "show only numbers (default)"
msgstr "Nur Ziffern zeigen (Standard)"

#: pysdk-gui.py:256 pysdk-image.py:167 pysdk-pdf.py:210 pysdk.py:159
msgid "incorrect number of arguments"
msgstr "Ungültige Anzahl der Argumente"

#: pysdk-gui.py:342
msgid "You have not psyco installed, if you can, install it to get better performance"
msgstr "Psyco nicht installiert, wenn verfügbar, bitte installierenfür bessere Performance"

#: pysdk-gui.py:344
msgid "You have not reportlab installed, necessary to save as PDF and printing"
msgstr "Reportlab ist nicht installiert, nötig zur Speicherung als PDF und zum Drucken"

#: pysdk-gui.py:346
msgid "You have not PIL installed, necessary to save as image"
msgstr "Python-Imaging-Library ist nicht installiert, nötig zur Speicherung als Bild"

#: pysdk-gui.py:348
msgid "You have not pygtk installed, necessary to gui"
msgstr "pygtk ist nicht installiert, wird von der grafischen Oberfläche benötigt"

#: pysdk-gui.py:365 pysdk-gui.py:367 pysdk-image.py:213 pysdk-image.py:215
#: pysdk-pdf.py:276 pysdk-pdf.py:278 pysdk.py:207 pysdk.py:209
msgid "\t-- cut here --"
msgstr "\t-- Ende --"

#: pysdk-gui.py:369 pysdk-image.py:217 pysdk-pdf.py:280 pysdk.py:211
#, python-format
msgid "An error has happened, please go to %s and send a bug report with the last lines."
msgstr "Ein Fehler ist aufgetreten, bitte gehen Sie zu %s und senden Sie einenFehlerreport mit den zuletzt ausgegebenen Zeilen."

#: pysdk-image.py:82
msgid ""
"\n"
"  %prog [Image Options] INPUT.sdk OUTPUT.format\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.format is an image file"
msgstr ""

#: pysdk-image.py:120 pysdk-pdf.py:149
#, python-format
msgid "set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-image.py:124 pysdk-pdf.py:153
#, python-format
msgid "set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default)"
msgstr ""

#: pysdk-image.py:128 pysdk-pdf.py:157
#, python-format
msgid "set the font size for the numbers (%d is the default)"
msgstr ""

#: pysdk-image.py:132 pysdk-pdf.py:161
#, python-format
msgid "set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-pdf.py:84
msgid ""
"\n"
"  %prog [PDF Options] [-p [Print Options]] INPUT.sdk [INPUT.sdk INPUT.sdk INPUT.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.pdf is a PDF file"
msgstr ""

#: pysdk-pdf.py:94
msgid "print a sudoku"
msgstr "Sudoku drucken"

#: pysdk-pdf.py:101
msgid "PDF Options"
msgstr ""

#: pysdk.py:81
msgid ""
"\n"
"  %prog INPUT.sdk [OUTPUT.sdk]\n"
"  %prog -c [Creation Options] OUTPUT.sdk\n"
"  %prog -t INPUT.sdk\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk and OUTPUT.sdk are Python Sudoku files"
msgstr ""

#: pysdk.py:92
msgid "create a sudoku"
msgstr "Sudoku erzeugen"

#: pysdk.py:96
msgid "test the difficulty of a sudoku"
msgstr ""

#: pysdk.py:110
msgid "be sure that the difficulty of the sudoku created is the difficulty given"
msgstr ""

#: pythonsudoku/gui.py:49
msgid "_Open"
msgstr "_Öffnen"

#: pythonsudoku/gui.py:54
msgid "_Save sudoku"
msgstr "_Spiel speichern"

#: pythonsudoku/gui.py:60
msgid "Save as P_DF"
msgstr "Als P_DF speichern"

#: pythonsudoku/gui.py:66
msgid "Save as _Image"
msgstr "Als B_ild speichern"

#: pythonsudoku/gui.py:73
msgid "_Print"
msgstr "_Drucken"

#: pythonsudoku/gui.py:78
msgid "_Quit"
msgstr "B_eenden"

#: pythonsudoku/gui.py:83
msgid "_File"
msgstr "_Datei"

#: pythonsudoku/gui.py:92
msgid "_Undo"
msgstr "_Rückgängig"

#: pythonsudoku/gui.py:97
msgid "_Redo"
msgstr "_Wiederholen"

#: pythonsudoku/gui.py:102
msgid "_Edit"
msgstr "_Bearbeiten"

#: pythonsudoku/gui.py:111
msgid "_Create"
msgstr "Er_zeugen"

#: pythonsudoku/gui.py:116
msgid "C_heck"
msgstr "Pr_üfen"

#: pythonsudoku/gui.py:121
msgid "_Solve"
msgstr "Auflö_sen"

#: pythonsudoku/gui.py:126
msgid "_Give one number"
msgstr "Eine _Zahl vorgeben"

#: pythonsudoku/gui.py:131
msgid "_Sudoku"
msgstr "_Sudoku"

#: pythonsudoku/gui.py:140
msgid "_About"
msgstr "_Über"

#: pythonsudoku/gui.py:145
msgid "_What is"
msgstr "_Was ist Sudoku ?"

#: pythonsudoku/gui.py:150
msgid "_Help"
msgstr "_Hilfe"

#: pythonsudoku/gui.py:173
msgid "Open file"
msgstr "Datei öffnen"

#: pythonsudoku/gui.py:183
msgid "Save file"
msgstr "Speichern"

#: pythonsudoku/gui.py:193
msgid "Save file as PDF"
msgstr "Als PDF speichern"

#: pythonsudoku/gui.py:203
msgid "Save file as an image"
msgstr "Als Bild speichern"

#: pythonsudoku/gui.py:218
msgid "Select a number"
msgstr "Zahl auswählen"

#: pythonsudoku/gui.py:394
msgid "Create sudoku"
msgstr "Sudoku erzeugen"

#: pythonsudoku/gui.py:398
msgid "Select your handicap"
msgstr "Handicap wählen"

#: pythonsudoku/gui.py:439 pythonsudoku/text.py:76
msgid "Creating sudoku..."
msgstr "Sudoku erzeugen..."

#: pythonsudoku/gui.py:616
msgid "Solved!"
msgstr "Gelöst!"

#: pythonsudoku/gui.py:648
msgid "This sudoku can be solved."
msgstr "Dieses Sudoku ist lösbar."

#: pythonsudoku/gui.py:650 pythonsudoku/gui.py:653
msgid "This sudoku can't be solved."
msgstr "Dieses Sudoku ist NICHT lösbar."

#: pythonsudoku/info.py:18
msgid ""
"Sudoku, sometimes spelled Su Doku, is a placement puzzle, also known as Number Place in the United States. The aim of the puzzle is to enter a numeral from 1 through 9 in each cell of a grid, most frequently a 9x9 grid made up of 3x3 subgrids (called \"regions\"), starting with various numerals given in some cells (the \"givens\"). Each row, column and region must contain only one instance of each numeral. Completing the puzzle requires patience and logical ability. Its grid layout is reminiscent of other newspaper puzzles like crosswords and chess problems. Sudoku initially became popular in Japan in 1986 and attained international popularity in 2005.\n"
"\n"
"More information in http://en.wikipedia.org/wiki/Sudoku"
msgstr ""
"Sudoku, auch Su Doku, ist ein Zahlenpuzzle. Ziel des Spiels ist es, in jede Zelle eines Gitters eine Ziffer von 1 bis 9 einzusetzen. Das Gitter besteht meist aus 9 mal 9 Feldern, wobei innerhalb des Gitters Quadrate von 3 mal 3 Feldern definiert sind. In jeder Zeile, Spalte und jedem der inneren Quadrate darf jede Ziffer nur ein einziges Mal vorkommen. Die Felderstruktur erinnert an andere Logikaufgaben aus Zeitungen, z. B. Kreuzworträtsel oder Schachaufgaben. Sudoku ist seit 1986 in Japan bekannt und erlangte 2005 weltweite Popularität. \n"
"\n"
"Mehr Informationen: http://de.wikipedia.org/wiki/Sudoku"

#: pythonsudoku/printer.py:33
msgid "Print command not set"
msgstr "Druckbefehl nicht gesetzt"

#: pythonsudoku/text.py:87
msgid "sudoku with wrong difficulty!"
msgstr ""

#: pythonsudoku/text.py:90 pythonsudoku/text.py:119
msgid "success!"
msgstr "Viel Erfolg!"

#: pythonsudoku/text.py:112
msgid "Solving sudoku..."
msgstr "Sudoku auflösen..."

#: pythonsudoku/text.py:122
msgid "can't be solved!"
msgstr "Nicht lösbar!"

#: pythonsudoku/text.py:138
msgid "The difficulty of the sudoku is..."
msgstr ""
